@extends('layouts.app')

@section('title', 'Users')

@section('content')

@if(Session::has('notallowedchange'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowedchange')}}
</div>
@endif

@if(Session::has('change'))
<div class = "alert alert-success" role="alert">
    {{Session::get('change')}}
</div>
@endif



<h1>List Of Users</h1>
<!-- class = table (by bootstrap)-->
<table class = "table">
    <tr>
        <th>Id</th><th>Name</th><th>Email</th><th>Department</th><th>Craeted</th><th>Updated</th><th>Details</th><th>Manager</th><th>Delete</th>
    </tr>
    <!-- the table data -->
    @foreach($users as $user)
        <tr>
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>
                @foreach(App\Role::roleName($user->id) as $role)
                    {{$role->name}}
                @endforeach
            <td>{{$user->created_at}}</td>
            <td>{{$user->updated_at}}</td>
            <td><a href ="{{route('users.show',$user->id)}}">Details</a></td>
            <td>
            @if(!$user->isManager())
            <a href ="{{route('users.makemanager',$user->id)}}">Make Manager</a>
            @else
            <a href ="{{route('users.cancelmanager',$user->id)}}">Cancel Permission</a>
            @endif
            </td>
            <!--href using only the GET method -->
            <td><a href ="{{route('users.delete',$user->id)}}">Delete</a></td>
            
        </tr>
    @endforeach
</table>
@endsection
