@extends('layouts.app')

@section('title', 'User Details')

@section('content')

<!-- @if(Session::has('notallowedchange'))
<div class = 'alert alert-danger'>
    {{Session::get('notallowedchange')}}
</div>
@endif -->


<h1>Details Of User</h1>
<!-- class = table (by bootstrap)-->
<table class = "table">
    <tr>
        <th>Id</th>
        <td>{{$user->id}}</td>
    </tr> 
    <tr>
        <th>Name</th>
        <td>{{$user->name}}</td>
    </tr>
    <tr>
        <th>Email</th>
        <td>{{$user->email}}</td>
    </tr>
    <tr>
        <th>Department</th>
        <td>

        @if(!$user->isAdmin())
            {{$user->department->name}}
        @else
            <form method="POST" action="{{ route('users.changedepartmentfromuser') }}">
            @csrf  
            <div class="form-group row">
            <label for="department_id" class="col-md-4 col-form-label text-md-right">Department</label>
            <div class="col-md-6">
                <select class="form-control" name="department_id">                                                                         
                    @foreach ($departments as $department)
                    <option value="{{ $department->id }}"> 
                        {{ $department->name }} 
                    </option>
                    @endforeach    
                </select>
            </div>
            <input name="id" type="hidden" value = {{$user->id}} >
            <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Change Department
                        </button>
                    </div>
            </div>                    
            </form> 
            </div>


        @endif    
        </td>
    </tr>
    <tr>
        <th>Permissions</th>
        <td>{{$user->roles}}</td>
    </tr>

    <tr>
        <th>Craeted</th>
        <td>{{$user->created_at}}</td>
    </tr>
    <tr>
        <th>Updated</th>
        <td>{{$user->updated_at}}</td>
    </tr>
    <tr>
        <th>Delete</th>
        <td><a href ="{{route('users.delete',$user->id)}}">Delete</a></td>
    </tr>

</table>

<!-- </div> -->


@endsection
