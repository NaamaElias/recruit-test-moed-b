<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role extends Model
{
    public function users(){
        return $this->belongsToMany('App\User','userroles'); // conect to users table by users_roles - connect table
    }



    public static function roleName($user_id){
        $roles = DB::table('userroles')->where('user_id',$user_id)->pluck('role_id');
        return self::find($roles)->all();
    }
}
