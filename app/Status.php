<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Status extends Model
{
    public function candidateStatus(){
        return $this->hasMany('App\Candidate') ;
    }

    public static function next($status_id){
        // SELECT to FROM nextstages WHERE from = $status_id
        $nextstages = DB::table('nextstages')->where('from',$status_id)->pluck('to') ; // we should insert the DB librrary
        return self::find($nextstages)->all(); // return the name status  from statuses table 
                                              // self relae to status class 
    }

    public static function allowed($from,$to){
        // SELECT FROM nextstages WHERE from = $from and to = $to
        $allowed = DB::table('nextstages')->where('from',$from)->where('to',$to)->get() ; 
        if(isset($allowed)) return true;  //if allowed has value -> true
        return false;
    }
 
}
