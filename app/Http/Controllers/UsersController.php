<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Department;
use Illuminate\Support\Facades\Gate;
use App\Candidate;
use Illuminate\Support\Facades\Session;
use App\Role;
use App\userRole;



class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $departments = Department::all();
        $roles = Role::all();
        return view('users.index', compact('users','departments','roles'));
    }


    public function changeDepartmentFromUser(Request $request){
        $uid = $request->id;
        $did = $request->department_id;
        if(Gate::allows('change-department'))
        {
            $user = User::findOrFail($uid);
            $user->department_id = $did;
            $user->save();
        }else{
            Session::flash('notallowedchange','You are not allowed to change the department of the user because the user has candidate');  
        }
        return redirect('users');
    }

    public function makeManager($uid){
        if(Gate::allows('make-manager'))
        {
            $user = User::findOrFail($uid);
            $user->department_id = '2';
            $user->save();
            Session::flash('change','Permission changed successfully');  
        }
        return redirect('users');
    }

    public function cancelManager($uid){
        if(Gate::allows('make-manager'))
        {
            $user = User::findOrFail($uid);
            $user->department_id = '1';
            $user->save();  
        }
        return redirect('users');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $departments = Department::all();
        $candidates = Candidate::all();
        $roles = $user->roles;

        return view('users.show', compact('user','departments','candidates','roles'));
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Gate::authorize('delete-user');
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('users');
    }
}
